﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ReadAllTex
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int opcion;            
            Console.WriteLine("Menu de antivirus");
            Console.WriteLine("1.- Buscar firma de virus en directorio ");
            Console.WriteLine("2.- Buscar firma que mas se repite en directorio");
            opcion = Convert.ToInt16(Console.ReadLine());
            
            
            
            

            switch (opcion)
            {
                    case 1:
                        VirusBySign();
                        break;
                    
                    case 2:
                        VirusByCommonString();
                        break;
                    
                    default:
                        Console.WriteLine("opcion invalida");
                        break;
            }
                    

        }

        private static void VirusBySign()
        {
            
            Console.WriteLine("Escriba la ruta que desea escanear: ");
            string path = Console.ReadLine();
            
            string[] files = Directory.GetFiles(path);
            foreach (string currentFile in files)
            {
            
                string text = string.Concat(System.IO.File.ReadAllText(currentFile));
                string virus = "mi nombre"; // <------- Escribir firma del virus aqui
                if (text.Contains(virus))
                    Console.WriteLine("archivo "+ Path.GetFileName(currentFile) + " infectado");
                else
                    Console.WriteLine("archivo "+ Path.GetFileName(currentFile) + " limpio");
            }
            
        }


        private static void VirusByCommonString()
        {
            Console.WriteLine("Escriba la ruta que desea escanear: ");
            String path = Console.ReadLine();
//                        Console.WriteLine(path.ToString());
//                        Console.ReadLine();
            Dictionary<String, int> virus = new Dictionary<string, int>();
            string[] files = Directory.GetFiles(path); // Lee todos los archivos en directorio
            foreach (string currentFile in files) // itera los archivos
            {
            
                string text = string.Concat(File.ReadAllText(currentFile)); // abre los archivos
                string[] splitText = text.Split(',', ' '); // separa el texto en palabras y por comas

                
                foreach (var word in splitText)
                {
                    

                    if (virus.ContainsKey(word))
                    {
                      
                        virus[word]++; //aumenta el valor del key en el diccionario
                       
                        
                    }
                    else
                    {
                        virus.Add(word, 1); // agrega key al diccionario con valor 1
                    }
                    
                } 


            }
            var max = virus.Aggregate((l, r) => l.Value > r.Value ? l : r).Key; // Encuentra mayor key dentro del diccionario
            Console.WriteLine("El virus es " + max);
        }

        
    }
}

